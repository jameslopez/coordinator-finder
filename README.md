# Coordinator Finder

This is a tool to find [coordinators](https://handbook.gitlab.com/handbook/engineering/development/processes/infra-dev-escalation/process/#coordinator) for the Development on-call escalation process

## Getting started

Please go to the [latest job page](https://gitlab.com/jameslopez/coordinator-finder/-/jobs) and select the latest `deploy` job to see the list of next potential coordinators (in ascending order by number of times the person has been a coordinator)

## License

MIT