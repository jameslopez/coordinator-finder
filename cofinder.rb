#!/usr/bin/env ruby
require 'dotenv/load' if defined? Dotenv
require 'gitlab'
require 'open-uri'
require 'yaml'
require 'pp'
require 'date'
require 'tty-table'

pastel = Pastel.new(enabled: true)
Gitlab.endpoint = ENV['GITLAB_ENDPOINT']
Gitlab.private_token = ENV['GITLAB_PRIVATE_TOKEN']

coordinators = Gitlab.group_issues('gitlab-com', labels: ['Engineering Management'], search: 'Dev on-call monthly for 20', per_page: 100).auto_paginate.map { |a| a.to_hash['assignees'].first&.dig('username') }.compact.tally

yaml_raw = URI("https://about.gitlab.com/company/team/team.yml").read
team = Psych.load_stream(yaml_raw).first
eng_managers = team.map { |a| a['gitlab'] if a['role'].include?('Engineering Manager') && a['departments'].any? { |s| s.include?('Development') } }.compact
all_coordinators = eng_managers.to_h { |h| [h, 0] }.merge(coordinators).sort_by(&:last)

plus_month = lambda { |months| Date::MONTHNAMES[(Date.today >> months + 1).month] }
top_headers = ['Next coordinator', 'Coordinator Times', 'Potential Month']
top_table = TTY::Table.new(top_headers, all_coordinators[0..2].zip(3.times.collect { |i| plus_month.call(i) }).collect(&:flatten))
filter = ->(val, row_index, _col_index) do
  if row_index == 0
    pastel.bold(val)
  else
    val
  end
end

puts pastel.green.bold('Next potential coordinators:')
puts top_table.render(:unicode, border: {style: :green}, filter: filter)
puts pastel.bold('Coordinators by number of times:')

all_table = TTY::Table.new(%w[Coordinator Times], all_coordinators)
puts all_table.render(:unicode, filter: filter)
